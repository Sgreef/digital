library IEEE;
use IEEE.std_logic_1164.all;
 
entity behavior_model is
	port(
		D0, D1, D2, D3 : in std_logic_vector(4 downto 0);
		a, b : in std_logic;
		out_3 : out std_logic_vector(4 downto 0)
	);
end behavior_model;

architecture behave of behavior_model is
begin
  process ( D0, D1, D2, D3, a, b ) begin
	if (a = '1') and (b = '1') then
			out_3 <= D3;
	elsif (a = '1') and (b = '0') then
			out_3 <= D1;
	elsif (a = '0') and (b = '0') then
			out_3 <= D0;
	else 
			out_3 <= D2;
	end if;
  end process;
end behave;