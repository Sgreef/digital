library IEEE;
use IEEE.std_logic_1164.all;
-- uus olem
entity data_flow_model is
	port(
		D0, D1, D2, D3 : in std_logic_vector(4 downto 0);
		a, b : in std_logic;
		out_3 : out std_logic_vector(4 downto 0)
	);
end data_flow_model;

architecture dataflow of data_flow_model is
signal a_5, b_5 : std_logic_vector(4 downto 0);
begin
process(a,b) begin 

	if a ='0' then
		a_5 <= "00000";
	else 
		a_5 <= "11111";
	end if;
	
	if b ='0' then
		b_5 <= "00000";
	else 
		b_5 <= "11111";
	end if;

end process;
-- mudel
	out_3 <= (((not a_5) and (not b_5) and D0)or((not a_5) and b_5 and D2)or(a_5 and (not b_5) and D1)or(a_5 and b_5 and D3));

end dataflow;