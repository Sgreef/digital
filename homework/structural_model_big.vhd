library IEEE;
use IEEE.std_logic_1164.all;
 
entity structural_model_big is
	port(
		D0, D1, D2, D3 : in std_logic_vector(0 to 4);
		a, b : in std_logic;
		out_3 : out std_logic_vector(0 to 4)
	);
end structural_model_big;
 
architecture structure of structural_model_big is
 
component structural_model is
	port(
		f, s : in std_logic_vector(0 to 4);
		c : in std_logic;
		y : out std_logic_vector(0 to 4)
	);
end component structural_model;
 
signal out_1, out_2 : std_logic_vector(0 to 4);
 
begin
	sm_1 : structural_model
		port map(
			f => D1,
			s => D0,
			c => a,
			y => out_1
		);
	sm_2 : structural_model
		port map(
			f => D3,
			s => D2,
			c => a,
			y => out_2
		);
	sm_3 : structural_model
		port map(
			f => out_2,
			s => out_1,
			c => b,
			y => out_3
		);
 
end structure;