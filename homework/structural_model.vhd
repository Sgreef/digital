library IEEE;
use IEEE.std_logic_1164.all;

entity structural_model is
	port(
		f, s : in std_logic_vector(4 downto 0);
		c : in std_logic;
		y : out std_logic_vector(4 downto 0)
	);
end structural_model;
-- kasutan komponenti
architecture struct of structural_model is
component NANDg
  port ( 
	x0, x1 : in std_logic_vector(4 downto 0);
        out_nand : out std_logic_vector(4 downto 0)
	);
end component;
 --abisignaalid
signal f_c, s_cc: std_logic_vector(4 downto 0);
signal c_c: std_logic_vector(4 downto 0);
signal c_5 : std_logic_vector(4 downto 0);



begin
	process(c) begin 
		if c ='1' then
			c_5 <= "11111";
		else 
			c_5 <= "00000";
		end if;
	end process;

	U1: NANDg 
	port map(
		x0=>c_5, 
		x1=>c_5, 
		out_nand=>c_c
		);

	U2: NANDg 
	port map(
		x0=>f, 
		x1=>c_5, 
		out_nand=>f_c
		);

	U3: NANDg 
	port map(
		x0=>c_c, 
		x1=>s,
		out_nand=>s_cc
		);

	U4: NANDg 
	port map(
		x0=>f_c,
		x1=>s_cc,
		out_nand=>y
		);
end struct;