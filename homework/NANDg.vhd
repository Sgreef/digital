library ieee;
use ieee.std_logic_1164.all;

entity NANDg is
   port( 
	x0, x1 : in std_logic_vector(4 downto 0);
        out_nand : out std_logic_vector(4 downto 0)
	);
end NANDg;

architecture behaviour of NANDg is 
begin
   out_nand <= not(x0 and x1);
end behaviour;